/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import java.util.LinkedList;


public class Contact implements Serializable {

    private String name;
    private String lastname;
    private String email;
    private LinkedList<String> phoneNumbers;

    public Contact() {
    }

    public Contact(String name, String lastname, String email, LinkedList<String> phoneNumbers) {
        this.name = name;
        this.lastname = lastname;
        this.email = email;
        this.phoneNumbers = phoneNumbers;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LinkedList<String> getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(LinkedList<String> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    @Override
    public String toString() {
        return "Contact{" + "name=" + name + ", lastname=" + lastname
                + ", email=" + email + ", phoneNumbers=" + phoneNumbers + '}';
    }

}
