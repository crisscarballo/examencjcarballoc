/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import models.Contact;
import views.MainFrame;
import views.PhonePanel;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;


public class MainFmeController {

    private static MainFmeController controller;
    private final MainFrame frame;
    private final MainFrameActionListener actionListener;

    private MainFmeController() {
        frame = new MainFrame();
        actionListener = new MainFrameActionListener();
    }

    public static MainFmeController getInstance() {
        return controller == null ? controller = new MainFmeController() : controller;
    }

    private void initController() {
        frame.getBtnAddPhoneNumber().addActionListener(actionListener);
        frame.getBtnDeletePhoneNumber().addActionListener(actionListener);
        frame.getBtnClear().addActionListener(actionListener);
        frame.getBtnOpen().addActionListener(actionListener);
        frame.getBtnSave().addActionListener(actionListener);
    }

    public void showMainFrame() {
        initController();
        frame.setVisible(true);
    }

    private class MainFrameActionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            switch (e.getActionCommand()) {
                case "AddPhoneNumber":
                    addNewPhoneNumberPanel();
                    break;
                case "DeletePhoneNumber":
                    deletePhoneNumberPanel();
                    break;
                case "Save":
                    saveContact();
                    break;
                case "Clear":
                    clear();
                    break;
                case "Open":
                    openContact();
                    break;
                default:
                    break;
            }
        }

        private void addNewPhoneNumberPanel() {
            frame.getPnlNumbers().add(new PhonePanel());
        }

        private void deletePhoneNumberPanel() {
            int indexToRemove = frame.getPnlNumbers().getComponentCount() - 1;
            if (indexToRemove >= 0) {
                frame.getPnlNumbers().remove(frame.getPnlNumbers().getComponent(indexToRemove));
                frame.getPnlNumbers().repaint();
                frame.getPnlNumbers().revalidate();
            }
        }

        private void clear() {
            frame.getTxtEmail().setText("");
            frame.getTxtSurname().setText("");
            frame.getTxtName().setText("");
            frame.getPnlNumbers().removeAll();
            frame.getPnlNumbers().repaint();
            frame.getPnlNumbers().revalidate();
        }

        private void saveContact() {
            Contact contact = new Contact();
            contact.setName(frame.getTxtName().getText());
            contact.setLastname(frame.getTxtSurname().getText());
            contact.setEmail(frame.getTxtEmail().getText());
            contact.setPhoneNumbers(getPhoneNumbers());
            
            saveFile(contact);
            clear();
        }

        private LinkedList<String> getPhoneNumbers() {
            LinkedList<String> phoneNumbers = new LinkedList<>();
            Component components[] = frame.getPnlNumbers().getComponents();
            PhonePanel panels[] = new PhonePanel[components.length];
            for (int i = 0; i < components.length; i++) {
                panels[i] = (PhonePanel) components[i];
            }
            String number;
            for (PhonePanel panel : panels) {
                number = String.valueOf(panel.getCmbBoxTypes().getSelectedItem()) + ","
                        + panel.getTxtPhoneNumber().getText();
                phoneNumbers.add(number);
            }
            return phoneNumbers;
        }

        private void saveFile(Contact contact) {
            File file;
            if (frame.getFileChooser().showSaveDialog(frame) == JFileChooser.APPROVE_OPTION) {
                file = frame.getFileChooser().getSelectedFile();
                try (ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream(file))) {
                    output.writeObject(contact);
                    output.flush();
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(MainFmeController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(MainFmeController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        private void openContact() {
            if (frame.getFileChooser().showOpenDialog(frame) == JFileChooser.APPROVE_OPTION) {
                File file = frame.getFileChooser().getSelectedFile();
                try (ObjectInputStream input = new ObjectInputStream(new FileInputStream(file))) {
                    try {
                        Contact contact = (Contact) input.readObject();
                        showContent(contact);
                    } catch (ClassNotFoundException ex) {
                        Logger.getLogger(MainFmeController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(MainFmeController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(MainFmeController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        private void showContent(Contact contact) {
            frame.getTxtName().setText(contact.getName());
            frame.getTxtSurname().setText(contact.getLastname());
            frame.getTxtEmail().setText(contact.getEmail());
            contact.getPhoneNumbers().stream().map(s -> {
                PhonePanel panel = new PhonePanel();
                panel.getCmbBoxTypes().setSelectedItem(s.split(",")[0]);
                panel.getTxtPhoneNumber().setText(s.split(",")[1]);
                return panel;
            }).forEachOrdered(panel -> {
                frame.getPnlNumbers().add(panel);
            });
        }
    }
}
